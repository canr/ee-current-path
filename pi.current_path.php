<?php
/**
 * ExpressionEngine plugin to backport the {current_path} global variable.
 * @author Eric Slenk <slenkeri@anr.msu.edu>
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
    'pi_name' => 'Current Path',
    'pi_version' => '1.0.0',
    'pi_author' => 'Eric Slenk',
    'pi_description' => 'Backports the {current_path} global variable from EE 2.9.',
    'pi_usage' => 'Insert "{exp:current_path}" in your template to get the current document\'s request URI.');

/**
 * ExpressionEngine plugin to return the current request URI.
 */
class Current_path
{
	/**
	 * Constructor.
	 *
	 * Sets the return_data member to the request URI.
	 */
    public function __construct()
    {
    	$this->return_data = $_SERVER['REQUEST_URI'];
    }
}

/* End of file pi.current_path.php */
/* Location: ./system/expressionengine/third_party/plugin_name/pi.current_path.php */